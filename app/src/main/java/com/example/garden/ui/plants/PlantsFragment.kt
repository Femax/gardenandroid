package com.example.garden.ui.plants

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.AdapterView.OnItemClickListener
import android.widget.LinearLayout
import android.widget.PopupWindow
import android.widget.Toast
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.garden.databinding.FragmentHomeBinding
import org.koin.android.ext.android.inject
import androidx.navigation.findNavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.fragment.findNavController
import com.example.garden.R
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayout.OnTabSelectedListener

interface ItemClickListener {
    fun onPlantClick(plant: Plant)
}

class PlantsFragment : Fragment(), ItemClickListener {

    private var _binding: FragmentHomeBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val plantsPresenter: PlantsPresenter by inject()

    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentHomeBinding.inflate(inflater, container, false)
        val root: View = binding.root
        val plants = plantsPresenter.getPlants()
        val plantTypes = plantsPresenter.getPlantTypes()
        val plantsAdapter = PlantsAdapter(plants.filter { it.plantType == 1 }, this)
//        Toast.makeText(context,plants.toString(),Toast.LENGTH_LONG).show()
        binding.plantsRecyclerView.layoutManager = LinearLayoutManager(context)
        binding.plantsRecyclerView.adapter = plantsAdapter

        binding.fabButton.setOnClickListener {
            val navController = this.findNavController()
            navController.navigate(R.id.navigation_add_plant)
        }

        plantTypes.forEach { it ->
            val tab = binding.tabLayout.newTab()
            tab.setText(it.title); // set the Text for the first Tab
            tab.setId(it.plantType)
            tab.setIcon(R.drawable.ic_baseline_bookmark_24); // set an icon for the first tab
            binding.tabLayout.addTab(tab); // add  the tab to the TabLayout
        }

        binding.tabLayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {

            override fun onTabSelected(tab: TabLayout.Tab?) {
                tab!!.id
                val plants = plantsPresenter.getPlants()
                val plantsAdapter =
                    PlantsAdapter(plants.filter { it.plantType == tab!!.id }, this@PlantsFragment)
                binding.plantsRecyclerView.layoutManager = LinearLayoutManager(context)
                binding.plantsRecyclerView.adapter = plantsAdapter
            }

            override fun onTabReselected(tab: TabLayout.Tab?) {

            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {

            }


        })

        return root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onPlantClick(plant: Plant) {
        Toast.makeText(context, plant.toString(), Toast.LENGTH_LONG).show()
        val navController = this.findNavController()
        val bundle = bundleOf("id" to plant.id.toString())
        Toast.makeText(context, bundle.toString(), Toast.LENGTH_LONG).show()
        navController.navigate(R.id.navigation_plant_info, bundle)
    }
}