package com.example.garden.ui.notifications.addnotification

import android.app.AlertDialog
import android.app.Notification
import android.os.Build
import android.os.Bundle
import android.text.SpannableStringBuilder
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.DatePicker
import android.widget.TimePicker
import androidx.annotation.RequiresApi
import androidx.fragment.app.Fragment
import com.example.garden.R
import com.example.garden.databinding.FragmentAddNotificationBinding
import com.example.garden.ui.dialog.PickPlantDialogFragment
import com.example.garden.ui.notifications.NotificationRepository
import com.example.garden.ui.plants.Plant
import com.example.garden.ui.plants.PlantsRepository
import org.koin.android.ext.android.inject
import java.sql.Date
import java.sql.Time
import java.util.*

class AddNotificationFragment : Fragment(), PickPlantDialogFragment.PickPlantDialogListener {

    private var _binding: FragmentAddNotificationBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!
    private lateinit var plantId: String
    private val notificationRepository: NotificationRepository by inject()
    private val plantsRepository: PlantsRepository by inject()
    private lateinit var plantPickerDialog: PickPlantDialogFragment
    private lateinit var datePicker: AlertDialog
    private lateinit var choosedPlant: Plant
    private lateinit var plants: List<Plant>
    private var choosedTime: Long = 0

    @RequiresApi(Build.VERSION_CODES.Q)
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View {
        _binding = FragmentAddNotificationBinding.inflate(inflater, container, false)
        val root: View = binding.root
        plants = plantsRepository.plants()
        choosedPlant = plants.get(0)
        binding.plantPicker.setOnClickListener { showPlantPickerDialog() }
        binding.datePickerButton.setOnClickListener { showDatePickerDialog() }
        binding.saveButton.setOnClickListener {
            val notification = com.example.garden.ui.notifications.Notification(
                time = Date(choosedTime),
                title = binding.title.editText!!.text.toString(),
                description = "",
                checked = false,
                plantId = choosedPlant.id!!
            )
            notificationRepository.saveNotification(notification)
        }
        return root
    }

    private fun showDatePickerDialog() {
        val dialogView = View.inflate(activity, R.layout.date_time_picker_dialog, null)
        datePicker = AlertDialog.Builder(activity).create()

        dialogView.findViewById<View>(R.id.date_time_set).setOnClickListener {
            val datePicker: DatePicker =
                dialogView.findViewById<View>(R.id.date_picker) as DatePicker
            val timePicker: TimePicker =
                dialogView.findViewById<View>(R.id.time_picker) as TimePicker
            val calendar: Calendar = GregorianCalendar(
                datePicker.getYear(),
                datePicker.getMonth(),
                datePicker.getDayOfMonth(),
                timePicker.getCurrentHour(),
                timePicker.getCurrentMinute()
            )
            choosedTime = calendar.getTimeInMillis()
            binding.datePicker.editText!!.text = SpannableStringBuilder(choosedTime.toString())
        }
        datePicker.setView(dialogView)
        datePicker.show()
    }

    fun showPlantPickerDialog() {
        plantPickerDialog = PickPlantDialogFragment(plants)
        plantPickerDialog.show(requireActivity().supportFragmentManager, "PICK_PLANT")
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onDialogVarianClick(plantName: String) {
        choosedPlant = plants.find { it.title == plantName }!!
        binding.plantPickerEditText.text = SpannableStringBuilder(choosedPlant.title)
        plantPickerDialog.dismiss()
    }
}