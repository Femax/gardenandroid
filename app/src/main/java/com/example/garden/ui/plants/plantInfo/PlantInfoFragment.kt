package com.example.garden.ui.plants.plantInfo

import android.graphics.*
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.core.content.res.ResourcesCompat
import androidx.fragment.app.Fragment
import com.example.garden.R
import com.example.garden.databinding.FragmentPlantInfoBinding
import com.example.garden.ui.plants.PlantsPresenter
import org.koin.android.ext.android.inject

class PlantInfoFragment : Fragment() {

    private var _binding: FragmentPlantInfoBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val plantsPresenter: PlantsPresenter by inject()
    private val binding get() = _binding!!
    private lateinit var plantId: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments
        Toast.makeText(context, arguments.toString(), Toast.LENGTH_LONG).show()
        this.plantId = arguments?.getString("id") ?: "0"
    }

    @RequiresApi(Build.VERSION_CODES.Q)
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentPlantInfoBinding.inflate(inflater, container, false)
        val root: View = binding.root
        val plant = plantsPresenter.getPlant(plantId)
        binding.plantsCardTitleView.text = plant?.title ?: "Не найден"
        binding.plantStatusView.colorFilter = PorterDuffColorFilter(getColor(plant?.color ?: 0), PorterDuff.Mode.MULTIPLY)
        return root
    }

    fun getColor(color: Int): Int {
      return  when (color) {
            1 ->         ResourcesCompat.getColor(getResources(), R.color.bookmark_orange, null)
            2 ->         ResourcesCompat.getColor(getResources(), R.color.bookmark_blue, null)
            3 ->         ResourcesCompat.getColor(getResources(), R.color.bookmark_green, null)
            4 ->         ResourcesCompat.getColor(getResources(), R.color.bookmark_lilac, null)
            else -> ResourcesCompat.getColor(getResources(), R.color.black, null)

        }

    }


    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}