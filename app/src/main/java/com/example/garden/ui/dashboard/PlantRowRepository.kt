package com.example.garden.ui.dashboard


data class PlantRow(
    val id: String,
    val color: Int
)

interface PlantRowRepository {
    fun findPlantRow(id: String): PlantRow?
    fun addNotifications(plantRows: List<PlantRow>)
    fun plantRows(): List<PlantRow>
}

class PlantRowRepositoryImpl : PlantRowRepository {

    var plantsRow = arrayListOf<PlantRow>(
        PlantRow("1", 1),
        PlantRow("2", 2),
        PlantRow("3", 3),
        PlantRow("4", 4)
    )

    override fun findPlantRow(id: String): PlantRow? {
        return plantsRow.firstOrNull { it.id == id }

    }

    override fun addNotifications(plantRows: List<PlantRow>) {
        this.plantsRow = (this.plantsRow + plantRows) as ArrayList<PlantRow>
    }

    override fun plantRows(): List<PlantRow> {
        return plantsRow
    }
}