package com.example.garden.ui.plants

class PlantsPresenter(private val plantsRepository: PlantsRepository) {
    fun getPlant(id: String): Plant? {
        return plantsRepository.findPlant(id)
    }

    fun getPlants(): List<Plant> {
        return plantsRepository.plants()
    }

    fun addPlant(plant: Plant): Plant {
        return plantsRepository.addPlant(plant)
    }

    fun getPlantTypes(): List<PlantType> {
        return plantsRepository.getPlantTypes()
    }
}