package com.example.garden.ui.dialog

import android.app.AlertDialog
import android.app.Dialog
import android.content.DialogInterface
import android.os.Bundle
import androidx.fragment.app.DialogFragment
import com.example.garden.R
import com.example.garden.ui.plants.Plant

class PickPlantDialogFragment(val plants: List<Plant>) : DialogFragment() {

    // Use this instance of the interface to deliver action events
    internal lateinit var listener: PickPlantDialogListener

    /* The activity that creates an instance of this dialog fragment must
     * implement this interface in order to receive event callbacks.
     * Each method passes the DialogFragment in case the host needs to query it. */
    interface PickPlantDialogListener {
        fun onDialogVarianClick(plantName: String)
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return activity?.let {
            val builder = AlertDialog.Builder(it)
            val names = plants.map { it.title }.toTypedArray()
            builder.setTitle(R.string.pick_plant)
                .setItems(names,
                    DialogInterface.OnClickListener { dialog, which ->
                        // The 'which' argument contains the index position
                        // of the selected item
                    })
            builder.create()
        } ?: throw IllegalStateException("Activity cannot be null")
    }
}