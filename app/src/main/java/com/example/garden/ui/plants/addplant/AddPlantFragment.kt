package com.example.garden.ui.plants.addplant

import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import androidx.fragment.app.Fragment
import com.example.garden.databinding.FragmentAddPlantBinding
import com.example.garden.ui.plants.Plant
import com.example.garden.ui.plants.PlantsPresenter
import com.example.garden.ui.plants.PlantsRepository
import org.koin.android.ext.android.inject

class AddPlantFragment : Fragment() {

    private var _binding: FragmentAddPlantBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!
    private lateinit var plantId: String
    private val plantsRepository: PlantsRepository by inject()
    private var choosedColor: Int = 0
    private var choosedType: Int = 0

    @RequiresApi(Build.VERSION_CODES.Q)
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentAddPlantBinding.inflate(inflater, container, false)
        val typePicker = binding.plantPicker
        typePicker.minValue = 0
        typePicker.maxValue = 1
        typePicker.displayedValues = arrayOf("Картофель", "Помидоры")

        val colorPicker = binding.colorPicker
        colorPicker.minValue = 0
        colorPicker.maxValue = 3
        colorPicker.displayedValues = arrayOf("Оранджевый", "Синий", "Зеленый", "Фиолетовый")

        colorPicker.setOnValueChangedListener { it, pos, value -> choosedColor = value }
        typePicker.setOnValueChangedListener { it, pos, value -> choosedType = value }


        binding.saveButton.setOnClickListener {
            val plant: Plant =
                Plant(
                    color = choosedColor,
                    title = binding.title.editText!!.text.toString(),
                    description = binding.description.editText!!.text.toString(),
                    plantType = choosedType
                )
            plantsRepository.addPlant(plant)
        }
        val root: View = binding.root
        return root
    }

//    fun getColor(color: Int): Int {
//        return  when (color) {
//            1 ->         ResourcesCompat.getColor(getResources(), R.color.bookmark_orange, null)
//            2 ->         ResourcesCompat.getColor(getResources(), R.color.bookmark_blue, null)
//            3 ->         ResourcesCompat.getColor(getResources(), R.color.bookmark_green, null)
//            4 ->         ResourcesCompat.getColor(getResources(), R.color.bookmark_lilac, null)
//            else -> ResourcesCompat.getColor(getResources(), R.color.black, null)
//
//        }
//
//    }


    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}