package com.example.garden.ui.notifications

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.garden.R

class NotificationAdapter(private val notifications: List<Notification>) :
    RecyclerView.Adapter<NotificationAdapter.ViewHolder>() {

    /**
     * Provide a reference to the type of views that you are using
     * (custom ViewHolder).
     */
    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val date: TextView
        val id: TextView
        val title: TextView
        val description: TextView
        val flag: CheckBox

        init {
            // Define click listener for the ViewHolder's View.
            id = view.findViewById(R.id.notification_id)
            date = view.findViewById(R.id.notification_date)
            description = view.findViewById(R.id.notification_describe)
            title = view.findViewById(R.id.notification_title)
            flag = view.findViewById(R.id.notification_flag)
        }
    }

    // Create new views (invoked by the layout manager)
    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        // Create a new view, which defines the UI of the list item
        val view = LayoutInflater.from(viewGroup.context)
            .inflate(R.layout.notification_item_view, viewGroup, false)
        return ViewHolder(view)
    }

    // Replace the contents of a view (invoked by the layout manager)
    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {

        // Get element from your dataset at this position and replace the
        // contents of the view with that element
        viewHolder.title.text = notifications[position].title
        viewHolder.date.text = notifications[position].time.toString()
        viewHolder.description.text = notifications[position].description
        viewHolder.id.text = position.toString()
        viewHolder.flag.setChecked(notifications[position].checked)
    }

    // Return the size of your dataset (invoked by the layout manager)
    override fun getItemCount() = notifications.size

}