package com.example.garden.ui.dashboard

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.res.ResourcesCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.garden.R
import com.example.garden.databinding.FragmentDashboardBinding
import com.example.garden.ui.notifications.NotificationRepository
import org.koin.android.ext.android.inject


class DashboardFragment : Fragment() {

    private var _binding: FragmentDashboardBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!
    private val plantRowRepository: PlantRowRepository by inject()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View {
//        val dashboardViewModel = ViewModelProvider(this).get(DashboardViewModel::class.java)

        _binding = FragmentDashboardBinding.inflate(inflater, container, false)
        val root: View = binding.root
        val plantRow = plantRowRepository.plantRows()
        val plantRowColored = plantRow.map { PlantRow(it.id, getColor(it.color)) }
        val plantRowAdapter =
            DashboardMapAdapter(plantRowColored)
        binding.dashboardMapView.layoutManager = LinearLayoutManager(context)
        binding.dashboardMapView.adapter = plantRowAdapter

        return root
    }


    fun getColor(color: Int): Int {
        return when (color) {
            1 -> ResourcesCompat.getColor(getResources(), R.color.bookmark_orange, null)
            2 -> ResourcesCompat.getColor(getResources(), R.color.bookmark_blue, null)
            3 -> ResourcesCompat.getColor(getResources(), R.color.bookmark_green, null)
            4 -> ResourcesCompat.getColor(getResources(), R.color.bookmark_lilac, null)
            else -> ResourcesCompat.getColor(getResources(), R.color.black, null)

        }

    }


    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}