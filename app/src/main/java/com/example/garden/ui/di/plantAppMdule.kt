package com.example.garden.ui.di


import com.example.garden.ui.dashboard.PlantRow
import com.example.garden.ui.dashboard.PlantRowRepository
import com.example.garden.ui.dashboard.PlantRowRepositoryImpl
import com.example.garden.ui.notifications.NotificationPresenter
import com.example.garden.ui.notifications.NotificationRepository
import com.example.garden.ui.notifications.NotificationRepositoryImpl
import com.example.garden.ui.plants.PlantsPresenter
import com.example.garden.ui.plants.PlantsRepository
import com.example.garden.ui.plants.PlantsRepositoryImpl
import com.example.garden.ui.plants.plantInfo.PlantInfoPresenter
import okhttp3.OkHttpClient
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

/**
 * App Components
 */
val appModule = module {
    // SplashViewModel for Splash View
//    viewModel { SplashViewModel(get(), get()) }
//
//    viewModel { WeatherViewModel(get(), get()) }
//
//    viewModel { (id: String) -> DetailViewModel(id, get(), get()) }

    // Weather Data Repository
    single<PlantsRepository> { PlantsRepositoryImpl() }
    single<NotificationRepository> { NotificationRepositoryImpl() }
    single<PlantRowRepository> { PlantRowRepositoryImpl() }
    single<Retrofit> { provideRetrofit(get()) }
    single<OkHttpClient> { provideOkHttpClient() }
    factory { PlantsPresenter(get()) }
    factory { PlantInfoPresenter(get()) }
    factory { NotificationPresenter(get()) }

}


fun provideRetrofit(okHttpClient: OkHttpClient): Retrofit {
    return Retrofit.Builder().baseUrl("localhost:8080").client(okHttpClient)
        .addConverterFactory(GsonConverterFactory.create()).build()
}

fun provideOkHttpClient(): OkHttpClient {
    return OkHttpClient().newBuilder().build()
}


// Gather all app modules
//val onlineWeatherApp = listOf(weatherAppModule, remoteDataSourceModule)
//val offlineWeatherApp = listOf(weatherAppModule, localAndroidDataSourceModule)
//val roomWeatherApp = listOf(weatherAppModule, localAndroidDataSourceModule, roomDataSourceModule)