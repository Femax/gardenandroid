package com.example.garden.ui.plants

import com.example.garden.ui.plants.api.PlantsApi

data class Plant constructor(
    val plantType: Int,
    val title: String,
    val description: String,
    val color: Int,
    val id: String? = null
)

data class PlantType constructor(val plantType: Int, val title: String)

interface PlantsRepository {
    fun findPlant(id: String): Plant?
    fun addPlants(users: List<Plant>)
    fun plants(): List<Plant>
    fun getPlantTypes(): List<PlantType>
    fun addPlant(plant: Plant): Plant
}

class PlantsRepositoryRetrofitImpl(private val planstApi: PlantsApi) : PlantsRepository {
    override fun findPlant(id: String): Plant? {
        return planstApi.getPlantById(id).execute().body()
    }

    override fun addPlants(plants: List<Plant>) {
        plants.forEach { planstApi.createPlant(it).execute() }
    }

    override fun plants(): List<Plant> {
        return planstApi.getPlants().execute().body()!!
    }

    override fun getPlantTypes(): List<PlantType> {
        return planstApi.getPlantTypes().execute().body()!!
    }

    override fun addPlant(plant: Plant): Plant {
        return planstApi.createPlant(plant).execute().body()!!
    }

}

class PlantsRepositoryImpl : PlantsRepository {

    private val plants = arrayListOf<Plant>(
        Plant(1, "Египетский картофель", "", 1, "1"),
        Plant(1, "Белорусский картофель", "", 2, "2"),
        Plant(1, "Картофель красный", "", 3, "3"),
        Plant(2, "Помидор бычье сердце", "", 4, "4")
    )

    private val plantTypes =
        arrayListOf<PlantType>(PlantType(1, "Картофель"), PlantType(2, "Помидоры"))


    override fun findPlant(id: String): Plant? {
        return plants.firstOrNull { it.id == id }
    }

    override fun addPlant(plant: Plant): Plant {
        plants.add(plant)
        return plant
    }

    override fun addPlants(users: List<Plant>) {
        plants.addAll(users)
    }

    override fun plants(): List<Plant> {
        return plants
    }

    override fun getPlantTypes(): List<PlantType> {
        return plantTypes
    }
}