package com.example.garden.ui.notifications

class NotificationPresenter(val notificationRepository: NotificationRepository) {
    fun findNotification(id: String): Notification? {
        return notificationRepository.findNotification(id)
    }

    fun addNotifications(notifiactions: List<Notification>) {
        notificationRepository.addNotifications(notifiactions)
    }

    fun notifications(): List<Notification> {
        return notificationRepository.notifications()
    }
}