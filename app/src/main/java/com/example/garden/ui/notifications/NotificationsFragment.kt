package com.example.garden.ui.notifications

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CalendarView
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.garden.R
import com.example.garden.databinding.FragmentNotificationsBinding
import org.koin.android.ext.android.inject
import java.util.Date
import java.util.*

class NotificationsFragment : Fragment() {

    private var _binding: FragmentNotificationsBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    private val notificationRepository: NotificationRepository by inject()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val notificationsViewModel =
            ViewModelProvider(this).get(NotificationsViewModel::class.java)

        _binding = FragmentNotificationsBinding.inflate(inflater, container, false)
        val root: View = binding.root
        val notifications = notificationRepository.notifications()

        val currentTime: Date = Calendar.getInstance().time ?: Date(0)
        val currentCal = Calendar.getInstance()
        currentCal.time = currentTime
        val currentDay: Int = currentCal.get(Calendar.DAY_OF_MONTH)
        val currentMonth: Int = currentCal.get(Calendar.MONTH)
        val currentYear: Int = currentCal.get(Calendar.YEAR)

        val notificationAdapter = NotificationAdapter(
            filterNotification(
                notifications,
                currentYear,
                currentMonth,
                currentDay
            )
        )
        binding.notificationCalendarListView.layoutManager = LinearLayoutManager(context)
        binding.notificationCalendarListView.adapter = notificationAdapter

        binding.calendarView.setOnDateChangeListener { calendarView: CalendarView, year: Int, month: Int, dayOfMonth: Int ->

            val notificationAdapterDCL =
                NotificationAdapter(filterNotification(notifications, year, month, dayOfMonth))
            binding.notificationCalendarListView.layoutManager = LinearLayoutManager(context)
            binding.notificationCalendarListView.adapter = notificationAdapterDCL
        }

        binding.fabButton.setOnClickListener { onAddButtonClick(it) }

        return root
    }

    fun onAddButtonClick(view: View) {
        val navController = this.findNavController()
        navController.navigate(R.id.action_navigation_notifications_to_navigation_add_notification)
    }

    fun isWithinRange(testDate: Date, startDate: Date, endDate: Date): Boolean {
        return !(testDate.before(startDate) || testDate.after(endDate))
    }

    fun filterNotification(
        notifications: List<Notification>,
        year: Int,
        month: Int,
        dayOfMonth: Int
    ): kotlin.collections.List<Notification> {
        return notifications.filter {

            val cal = Calendar.getInstance()
            cal.time = it.time

            val notificationDay: Int = cal.get(Calendar.DAY_OF_MONTH)
            val notificationMonth: Int = cal.get(Calendar.MONTH)
            val notificationYear: Int = cal.get(Calendar.YEAR)

            return@filter notificationDay == dayOfMonth && notificationMonth == month && notificationYear == year
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}