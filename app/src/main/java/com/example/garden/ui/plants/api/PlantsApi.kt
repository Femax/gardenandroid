package com.example.garden.ui.plants.api

import com.example.garden.ui.plants.Plant
import com.example.garden.ui.plants.PlantType
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Path


interface PlantsApi {

    @GET("plants")
    fun getPlants(): Call<List<Plant>>

    @GET("plants/{id}")
    fun getPlantById(@Path("id") id: String): Call<Plant>

    @POST("plants")
    fun createPlant(@Body plant: Plant?): Call<Plant>

    @GET("plants/types")
    fun getPlantTypes(): Call<List<PlantType>>

}



