package com.example.garden.ui.notifications

import java.sql.Date

data class Notification(
    val time: Date,
    val title: String,
    val description: String,
    val checked: Boolean,
    val plantId: String,
    val id: String? = null
)

interface NotificationRepository {
    fun findNotification(id: String): Notification?
    fun addNotifications(notifiactions: List<Notification>)
    fun notifications(): List<Notification>
    fun saveNotification(notification: Notification)
}

class NotificationRepositoryImpl : NotificationRepository {

    private var notifications = mutableListOf<Notification>(
        Notification(Date(1672002999999), "Египетский картофель", "Полив", false, "1", "1"),
        Notification(Date(1672002999999), "Белорусский картофель", "Полив", true, "1", "2"),
        Notification(Date(1672002999999), "Картофель красный", "Полив", false, "1", "3"),
        Notification(Date(1672002999999), "Картофель обычный", "Полив", true, "1", "4")
    )

    override fun findNotification(id: String): Notification? {
        return notifications.first { it.id == id }
    }

    override fun addNotifications(notifiactions: List<Notification>) {
        this.notifications =
            (this.notifications + ArrayList<Notification>(notifiactions)) as ArrayList<Notification>
    }

    override fun notifications(): List<Notification> {
        return notifications
    }

    override fun saveNotification(notification: Notification) {
        notifications.add(notification)
    }
}