package com.example.garden.ui.dashboard

import android.graphics.PorterDuff
import android.graphics.PorterDuffColorFilter
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageButton
import androidx.recyclerview.widget.RecyclerView
import com.example.garden.R

class DashboardMapAdapter(private val plantRows: List<PlantRow>) :
    RecyclerView.Adapter<DashboardMapAdapter.ViewHolder>() {

    /**
     * Provide a reference to the type of views that you are using
     * (custom ViewHolder).
     */
    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val button1: ImageButton
        val button2: ImageButton
        val button3: ImageButton
        val button4: ImageButton
        val button5: ImageButton

        init {
            // Define click listener for the ViewHolder's View.
            button1 = view.findViewById(R.id.button1)
            button2 = view.findViewById(R.id.button2)
            button3 = view.findViewById(R.id.button3)
            button4 = view.findViewById(R.id.button4)
            button5 = view.findViewById(R.id.button5)

        }
    }

    // Create new views (invoked by the layout manager)
    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        // Create a new view, which defines the UI of the list item
        val view = LayoutInflater.from(viewGroup.context)
            .inflate(R.layout.plants_row_item_view, viewGroup, false)
        return ViewHolder(view)
    }

    // Replace the contents of a view (invoked by the layout manager)
    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {

        // Get element from your dataset at this position and replace the
        // contents of the view with that element
        viewHolder.button1.colorFilter =
            PorterDuffColorFilter(plantRows[position].color, PorterDuff.Mode.MULTIPLY)
        viewHolder.button2.colorFilter =
            PorterDuffColorFilter(plantRows[position].color, PorterDuff.Mode.MULTIPLY)
        viewHolder.button3.colorFilter =
            PorterDuffColorFilter(plantRows[position].color, PorterDuff.Mode.MULTIPLY)
        viewHolder.button4.colorFilter =
            PorterDuffColorFilter(plantRows[position].color, PorterDuff.Mode.MULTIPLY)
        viewHolder.button5.colorFilter =
            PorterDuffColorFilter(plantRows[position].color, PorterDuff.Mode.MULTIPLY)
    }


    // Return the size of your dataset (invoked by the layout manager)
    override fun getItemCount() = plantRows.size

}